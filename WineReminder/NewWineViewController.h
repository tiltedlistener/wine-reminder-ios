//
//  NewWineViewController.h
//  WineReminder
//
//  Created by Corey Kahler on 6/8/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SavedWine.h"

@interface NewWineViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic) SavedWine *wineToEdit;
@property (weak, nonatomic) IBOutlet UITextField *wineName;
@property (weak, nonatomic) IBOutlet UISegmentedControl *wineType;
@property (weak, nonatomic) IBOutlet UISlider *winePrice;
@property (weak, nonatomic) IBOutlet UILabel *displayedWinePrice;

- (IBAction)priceChanged:(id)sender;
- (IBAction)wineTypeChanged:(id)sender;
- (IBAction)saveWine:(id)sender;

@end
