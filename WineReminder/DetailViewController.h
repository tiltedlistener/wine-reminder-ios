//
//  DetailViewController.h
//  WineReminder
//
//  Created by Corey Kahler on 6/8/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "SavedWine.h"
#import <CoreData/CoreData.h>

@interface DetailViewController : ViewController

@property (nonatomic) SavedWine *wineDetail;
@property (weak, nonatomic) IBOutlet UILabel *nameDisplay;
@property (weak, nonatomic) IBOutlet UILabel *typeDisplay;
@property (weak, nonatomic) IBOutlet UILabel *priceDisplay;

- (IBAction)deleteWine:(id)sender;

@end
