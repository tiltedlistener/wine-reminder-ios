//
//  DetailViewController.m
//  WineReminder
//
//  Created by Corey Kahler on 6/8/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import "DetailViewController.h"
#import "NewWineViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize wineDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Wine Details";

    self.nameDisplay.text = wineDetail.name;
    self.priceDisplay.text = [NSString stringWithFormat:@"%.0f", wineDetail.price];
    self.typeDisplay.text = wineDetail.type;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)deleteWine:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if ([appDelegate deleteWine:self.wineDetail]) {
        [self performSegueWithIdentifier:@"deleteWine" sender:self];
    }     
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"editWine"])
    {
        NewWineViewController *vc = [segue destinationViewController];
        
        if (self.wineDetail != nil) {
            vc.wineToEdit = self.wineDetail;
        }
    }
}

@end
