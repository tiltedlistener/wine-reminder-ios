//
//  NewWineViewController.m
//  WineReminder
//
//  Created by Corey Kahler on 6/8/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import "NewWineViewController.h"
#import "AppDelegate.h"

@interface NewWineViewController ()

@end

@implementation NewWineViewController {
    SavedWine *newWine;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    newWine = [[SavedWine alloc] init];
    
    if (self.wineToEdit != nil) {
        newWine.name = self.wineToEdit.name;
        newWine.type = self.wineToEdit.type;
        newWine.price = self.wineToEdit.price;
        
        [self.wineName setText:newWine.name];
        [self.winePrice setValue:newWine.price animated:YES];
        [self.wineType setSelectedSegmentIndex: [newWine getSegementIndex]];
    }
    
    float sliderValue = [self.winePrice value];
    self.displayedWinePrice.text = [NSString stringWithFormat:@"%.0f", sliderValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.wineName resignFirstResponder];
    return YES;
}

#pragma mark - Outlets

- (IBAction)priceChanged:(id)sender {
    float sliderValue = [self.winePrice value];
    self.displayedWinePrice.text = [NSString stringWithFormat:@"%.0f", sliderValue];
}


- (IBAction)wineTypeChanged:(id)sender {

}

- (IBAction)saveWine:(id)sender {
    newWine.name = self.wineName.text;
    
    NSInteger index = [self.wineType selectedSegmentIndex];
    NSString *type = [self.wineType titleForSegmentAtIndex:index];
    newWine.type = type;
    
    float sliderValue = [self.winePrice value];
    newWine.price = sliderValue;
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    if (self.wineToEdit == nil) {
        [appDelegate.wines addObject:newWine];
        
        // Save to database
        NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Wine" inManagedObjectContext:managedObjectContext];
        NSManagedObject *newWineCD = [[NSManagedObject alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:managedObjectContext];
        
        [newWineCD setValue:self.wineName.text forKey:@"name"];
        [newWineCD setValue:[NSNumber numberWithFloat:sliderValue] forKey:@"price"];
        [newWineCD setValue:type forKey:@"type"];
        
        NSError *error = nil;
        
        if (![newWineCD.managedObjectContext save:&error]) {
            NSLog(@"Unable to save managed object context.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
    } else {
        NSManagedObject *editingWine = (NSManagedObject *)_wineToEdit;
        [editingWine setValue:newWine.name forKey:@"name"];

        // TODO - update all keys
        // TODO - Update wine object
        // TODO - update wine array
        
        NSError *saveError = nil;
        if (![editingWine.managedObjectContext save:&saveError]) {
            NSLog(@"Unable to save managed object context.");
            NSLog(@"%@, %@", saveError, saveError.localizedDescription);
        }
    }

    // Finally Segue
    [self performSegueWithIdentifier:@"saveWineAndReturn" sender:self];
}

@end
