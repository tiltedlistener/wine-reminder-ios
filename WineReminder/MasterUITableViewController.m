//
//  MasterUITableViewController.m
//  WineReminder
//
//  Created by Corey Kahler on 6/8/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import "MasterUITableViewController.h"
#import "DetailViewController.h"
#import "SavedWine.h"
#import "AppDelegate.h"

@interface MasterUITableViewController ()

@end

@implementation MasterUITableViewController {
    NSMutableArray *wines;
    SavedWine *currentWine;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    wines = [[NSMutableArray alloc] init];

    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    wines = appDelegate.wines;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return wines.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"TableCellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    SavedWine *wine = [wines objectAtIndex:indexPath.row];
    
    if (wine != nil) {
        cell.textLabel.text = wine.name;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SavedWine *wine = [wines objectAtIndex:indexPath.row];
    if (wine != nil) {
        currentWine = wine;
    }
    [self performSegueWithIdentifier:@"toDetailSegue" sender:self];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"toDetailSegue"])
    {
        DetailViewController *vc = [segue destinationViewController];
        
        if (currentWine != nil) {
            [vc setWineDetail:currentWine];
        } 
    }
}

@end
