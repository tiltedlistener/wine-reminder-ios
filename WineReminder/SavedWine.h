//
//  SavedWine.h
//  WineReminder
//
//  Created by Corey Kahler on 6/8/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SavedWine : NSObject

@property NSString *name;
@property float price;
@property NSString *type;

- (int)getSegementIndex;

@end
