//
//  SavedWine.m
//  WineReminder
//
//  Created by Corey Kahler on 6/8/16.
//  Copyright (c) 2016 Corey Kahler. All rights reserved.
//

#import "SavedWine.h"

@implementation SavedWine

- (int)getSegementIndex
{
    if ([self.type isEqualToString:@"Red"]) {
        return 0;
    } else if ([self.type isEqualToString:@"White"]) {
        return 1;
    } else if ([self.type isEqualToString:@"Rose"]) {
        return 2;
    } else if ([self.type isEqualToString:@"Bubbly"]) {
        return 3;
    }
    
    return 0;
}

@end
